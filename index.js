const express = require('express');
const cors = require('cors');
const mysqlDb = require('./mysqlDb');
const categories = require('./app/categories');
const items = require('./app/items');
const places = require('./app/places');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/categories', categories);
app.use('/items', items);
app.use('/places', places);

const port = 8000;



const run = async () => {
    await mysqlDb.connect();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};

run().catch(console.error);