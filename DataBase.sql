create schema office collate utf8_general_ci;

use office;

create table items
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    category_id int          null,
    place_id    int          null,
    description text         null,
    date        date         null,
    constraint items_categories_id_fk
        foreign key (category_id) references office.categories (id)
            on update cascade,
    constraint items_places_id_fk
        foreign key (place_id) references office.places (id)
            on update cascade
);




create table categories
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         null
);




create table places
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         null
);
