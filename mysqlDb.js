const mysql = require('mysql2/promise');
let connection = null;

module.exports = {
    connect: async () => {
        connection = await mysql.createConnection({
            host: 'localhost',
            user: 'admin',
            password: '0700202801',
            database: 'office',
        });
    },
    getConnection: () => connection
}