const express = require('express');
const mysqlDb = require('../mysqlDb');



const router = express.Router();

router.get('/', async (req, res) => {
    const [categories] = await mysqlDb.getConnection().query('SELECT * FROM categories');
    res.send(categories)
});

router.get('/:id', async (req, res) => {
    const [categories] = await mysqlDb.getConnection().query('SELECT * FROM categories WHERE id = ?', [req.params.id]);
    const category = categories[0];
    res.send(category);
});

router.post('/', async (req, res) => {
    const category = req.body;
if (category.title === '') {
    res.status(400).send({"error": "Id can not be empty"});
} else  {
    const [result] = await mysqlDb.getConnection().query(
        'INSERT INTO categories (title , description) VALUES (?, ?)',
        [category.title, category.description]
    );
    res.send({...category, id: result.insertId});
}



});

router.delete('/:id',async (req,res) => {
    const [categories] = await mysqlDb.getConnection().query('DELETE FROM categories WHERE id = ?', [req.params.id]);
    const category = categories[0]
    res.send(category);
});

router.put('/:id', async (req, res) => {
    const category = req.body;
    if (category.title === '') {
        res.status(400).send({"error": "Id can not be empty"});
    } else {
        const result = await mysqlDb.getConnection().query(
            'UPDATE categories SET ? WHERE id = ?', [req.body, req.params.id]
        );
        res.send(req.body);
    }
});


module.exports = router;