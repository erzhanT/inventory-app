const express = require('express');
const mysqlDb = require('../mysqlDb');


const router = express.Router();

router.get('/', async (req, res) => {
    const [items] = await mysqlDb.getConnection().query('SELECT * FROM items');
    res.send(items)
});

router.get('/:id', async (req, res) => {
    const [items] = await mysqlDb.getConnection().query('SELECT * FROM items WHERE id = ?', [req.params.id]);
    const item = items[0];
    res.send(item);
});

router.post('/', async (req, res) => {
    const item = req.body;

    if (item.title === '' || item.category_id === '' || item.place_id === '') {
        res.status(400).send({"error": "Title and id can not be empty"});
    } else {
        const [result] = await mysqlDb.getConnection().query(
            'INSERT INTO items (title , description, date, category_id, place_id) VALUES (?, ?, ?, ?, ?)',
            [item.title, item.description, item.date, item.category_id, item.place_id]
        );
        res.send({...item, id: result.insertId});
    }

});

router.delete('/:id', async (req, res) => {
    const [items] = await mysqlDb.getConnection().query('DELETE FROM items WHERE id = ?', [req.params.id]);
    const item = items[0]
    res.send(item);
});

router.put('/:id', async (req, res) => {
    const item = req.body;

    if (item.title === '' || item.category_id === '' || item.place_id === '') {
        res.status(400).send({"error": "Title and id can not be empty"});
    } else {
        const result = await mysqlDb.getConnection().query(
            'UPDATE items SET ? WHERE id = ?', [req.body, req.params.id]
        );
        res.send(req.body);
    }

});

module.exports = router;