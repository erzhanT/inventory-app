const express = require('express');
const mysqlDb = require('../mysqlDb');


const router = express.Router();

router.get('/', async (req, res) => {
    const [places] = await mysqlDb.getConnection().query('SELECT * FROM places');
    res.send(places)
});

router.get('/:id', async (req, res) => {
    const [places] = await mysqlDb.getConnection().query('SELECT * FROM places WHERE id = ?', [req.params.id]);
    const place = places[0];
    res.send(place);
});

router.post('/', async (req, res) => {
    const place = req.body;
    if (place.title === '') {
        res.status(400).send({"error": "Id can not be empty"});
    } else {
        const [result] = await mysqlDb.getConnection().query(
            'INSERT INTO places (title , description) VALUES (?, ?)',
            [place.title, place.description]
        );
        res.send({...place, id: result.insertId});
    }
});

router.delete('/:id', async (req, res) => {
    const [places] = await mysqlDb.getConnection().query('DELETE FROM places WHERE id = ?', [req.params.id]);
    const place = places[0]
    res.send(place);
});

router.put('/:id', async (req, res) => {
    const place = req.body;
    if (place.title === '') {
        res.status(400).send({"error": "Id can not be empty"});
    } else {
        const result = await mysqlDb.getConnection().query(
            'UPDATE places SET ? WHERE id = ?', [req.body, req.params.id]
        );
        res.send(req.body);
    }
});

module.exports = router;